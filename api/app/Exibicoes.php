<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exibicoes extends Model
{
    protected $fillable = ['id', 'data','hora','codcliente'];
    protected $hidden = ['id'];
    protected $table = 'exibicoes';    
    
    public function Exibicoes(){
        return $this->hasMany('App\Exibicoes');
    }
}
