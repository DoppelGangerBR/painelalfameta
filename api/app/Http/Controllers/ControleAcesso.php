<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ControleAcesso extends Controller
{
    public function AcessoPermitido(){
        $data = json([ 'Acesso' => 'Permitido']);
        return response()->json(compact('data'),200);
    }
    public function AcessoNegado(){
        $data = json([ 'Acesso' => 'Negado']);
        return response()->json(compact(data), 200);
    }
}
