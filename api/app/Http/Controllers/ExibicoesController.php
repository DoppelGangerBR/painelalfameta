<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Exibicoes;
use App\Usuario;
use Illuminate\Support\Facades\DB;
class ExibicoesController extends Controller
{
    public function index(){
        $Exibicoes = Exibicoes::get();
        return response()->json($Exibicoes);
    }

    public function ExibePorCliente($Token){
        $Usuario;        
        try{
            $Usuario = DB::table('usuario')->where('auth_token', '=', $Token)->pluck('codcliente');
            if($Usuario){                
                $Exibicoes = Exibicoes::where('codcliente', '=', $Usuario)->take(1000)->orderby('data','desc')->orderby('hora','desc')->get();
                if($Exibicoes){
                    return response()->json($Exibicoes);
                }else{
                    return response()->json(['msg'=>'Nenhum histórico encontrado']);
                }
            }
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(["erro"=>$e]);
        }catch(\Exception $ex){
            return response()->json(["erro"=>$ex]);
        }
    }
    
    public function RetornaTotais($Token){
        $Usuario;
        $hoje = date('d-m-Y');
        $semana = date('d-m-Y', \strtotime('-1 week'));        
        $mes = date('d-m-Y', \strtotime('-1 month'));        
        try{
            $Usuario = DB::table('usuario')->where('auth_token', '=', $Token)->pluck('id');
            if($Usuario){                
                $ExibicoesHoje = DB::table('exibicoes')->where([['codcliente','=', $Usuario],['data','=', $hoje]])->count();
                $ExibicoesSemana = DB::table('exibicoes')->where([['codcliente','=', $Usuario],['data','>=', $semana]])->count();
                $ExibicoesMes = DB::table('exibicoes')->where([['codcliente','=', $Usuario],['data','>=', $mes]])->count();
                $ExibicoesTotal = DB::table('exibicoes')->where('codcliente','=', $Usuario)->count();                
                return response()->json(['hoje'=> $ExibicoesHoje, 'mes'=>$ExibicoesMes, 'semana'=>$ExibicoesSemana, 'total'=>$ExibicoesTotal]);                
                }else{
                    return response()->json(['msg'=>'Nenhum histórico encontrado']);
                }                        
        }catch(\Illuminate\Database\QueryException $e){
            return response()->json(['erro'=>$e, 'usuario'=>$Usuario]);
        }
    }

}