<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    Route::get('/', function(){
        return response()->json(['message' => 'Usuarios API', 'Status' => 'Connected']);;
    });
    Route::post('Usuario', 'UsuarioController@store');
   
    Route::post('auth/login','AutorizacaoController@login');
    Route::post('auth/logout','AutorizacaoController@logout');
    Route::post('auth', 'AutorizacaoController@ValidaToken');

    
    //Tudo o que estiver dentro do grupo abaixo é protegido por token
    Route::group(['middleware' => ['jwt.verify']], function(){
        Route::get('principal', 'AutorizacaoController@closed');        
        Route::post('authadm', 'AutorizacaoController@VerificaSeEAdministrador');
        Route::put('usuario/desativa/', 'UsuarioController@desativaUsuario');        
        Route::resource('usuario', 'UsuarioController');        
        Route::resource('exibicoes','ExibicoesController');
        Route::get('filtros','MasterController@retornaDistinto');
        Route::get('master','MasterController@index');
        Route::get('historico/{token}','ExibicoesController@ExibePorCliente');
        Route::get('totais/{token}','ExibicoesController@RetornaTotais');
    });
    
    

Route::get('/', function(){
    return redirect('api');
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
