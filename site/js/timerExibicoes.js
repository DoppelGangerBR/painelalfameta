function startTimer() {    
    display = document.getElementById("timerProximaExibicao");
    var timer = 1200, minuto, segundo;
    
    var segundos = setInterval(function () {
        
        minuto = parseInt(timer / 60, 10);
        segundo = parseInt(timer % 60, 10);

        minuto = minuto < 10 ? "0" + minuto : minuto;
        segundo = segundo < 10 ? "0" + segundo : segundo;
        if (--timer < 0) {
            display.textContent = 'EXIBINDO VT';
            setTimeout(function(){
                timer = 5;
                location.reload();
            },10000);            
        }else{
            display.textContent = minuto + ":" + segundo;  
        }
    }, 1000);
}